package gov.nasa.jpl.mbee.util;

public interface HasId {
  public int getId();
}
